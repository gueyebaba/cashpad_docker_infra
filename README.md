**LAMP stack built with Docker Compose**

This is a basic LAMP stack environment built using Docker Compose. It consists following:

PHP 7.4

Apache 2.4

MySQL 5.7

**Installation**

1 - docker-compose build

2 - docker-compose up -d

Your LAMP stack is now ready!! You can access it via http://localhost

**Configuration Variables**

There are following configuration variables available and you can customize them by overwritting in your own .env file.

DOCUMENT_ROOT

It is a document root for Apache server. The default value for this is ./www. All your sites will go here and will be synced automatically.

MYSQL_DATA_DIR

This is MySQL data directory. The default value for this is ./data/mysql. All your MySQL data files will be stored here.

VHOSTS_DIR

This is for virtual hosts. The default value for this is ./config/vhosts. You can place your virtual hosts conf files here.

Make sure you add an entry to your system's hosts file for each virtual host.

APACHE_LOG_DIR

This will be used to store Apache logs. The default value for this is ./logs/apache2.

MYSQL_LOG_DIR

This will be used to store Apache logs. The default value for this is ./logs/mysql.

**Web Server**

Apache is configured to run on port 80. So, you can access it via http://localhost.

Apache Modules

By default following modules are enabled.

rewrite

headers

If you want to enable more modules, just update ./bin/webserver/Dockerfile.

You have to rebuild the docker image by running docker-compose build and restart the docker containers.

**Connect via SSH**

You can connect to web server using docker exec command to perform various operation on it. Use below command to login to container via ssh.

docker exec -it api-server /bin/bash

**PHP**

The installed version of PHP is 7.4


